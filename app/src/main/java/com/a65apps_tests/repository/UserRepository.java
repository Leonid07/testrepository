package com.a65apps_tests.repository;

import android.app.Application;
import android.icu.text.SimpleDateFormat;
import android.os.Build;
import android.util.Log;

import androidx.annotation.RequiresApi;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.a65apps_tests.AppsDatabase;
import com.a65apps_tests.model.SpecialtyUser;
import com.a65apps_tests.model.SpecialtyUserDao;
import com.a65apps_tests.model.User;
import com.a65apps_tests.model.UserDao;
import com.a65apps_tests.model.UserSpecialty;
import com.a65apps_tests.networking.RetrofitApi;
import com.a65apps_tests.networking.RetrofitService;
import com.a65apps_tests.networking.model.Example;
import com.a65apps_tests.networking.model.Response;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.regex.Pattern;

import retrofit2.Call;
import retrofit2.Callback;

public class UserRepository {

    final String TAG = "myLog";
    private UserDao mUserDao;
    private SpecialtyUserDao mSpecialtyUserDao;
    private LiveData<List<User>> AllUser;
    private LiveData<List<UserSpecialty>> AllSpecialty;
    private RetrofitApi retrofitApi;
    private UserRepository userRepository;
    private  String norm_name;
    private String norm_lname;
    private String birthday;


   public UserRepository getInstance() {
        if (userRepository == null) {
            userRepository = new UserRepository();
        }
        return userRepository;
    }

    public UserRepository() {
        retrofitApi = RetrofitService.cteateService(RetrofitApi.class);
    }

    public UserRepository(Application application) {
        AppsDatabase db = AppsDatabase.getAppsDatabase(application);
        mUserDao = db.userDao();
        AllUser = mUserDao.getAll();
        mSpecialtyUserDao = db.specialtyUserDao();
       // AllSpecialty = mSpecialtyUserDao.getUserSpecialty();
        retrofitApi = RetrofitService.cteateService(RetrofitApi.class);
    }

    public LiveData<List<User>> getAllUser() {
        return AllUser;
    }

    public LiveData<List<UserSpecialty>> getAllSpecialty(){

        return AllSpecialty;
    }

    public MutableLiveData<Response> getNews() {
        MutableLiveData<Response> newsData = new MutableLiveData<>();
        retrofitApi.example()
                .enqueue(new Callback<Example>() {
                             @RequiresApi(api = Build.VERSION_CODES.N)
                             @Override
                             public void onResponse(Call<Example> call, retrofit2.Response<Example> response) {
                                 Example example = response.body();

                                 int siz  = example.response.size();

                                 ArrayList<String> User_mass = new ArrayList<>();
                                 ArrayList<String> mass_date = new ArrayList<>();

                                 for(int i = 0; i<siz; i++) {
                                     int num = example.response.get(i).specialty.size();
                                     if(num>1){
                                         for(int l = 0; l<num; l++){

                                             String name_rand_case = example.response.get(i).fName;
                                             String lname_rand_case = example.response.get(i).lName;

                                             String name_Low = name_rand_case.toLowerCase();
                                             String lname_Low = lname_rand_case.toLowerCase();

                                             StringBuilder builder = new StringBuilder(name_Low);
                                             if (Character.isAlphabetic(name_Low.codePointAt(0))){
                                                 builder.setCharAt(0, Character.toUpperCase(name_Low.charAt(0)));
                                                 norm_name = builder.toString();
                                             }

                                             StringBuilder builder_lname = new StringBuilder(lname_Low);
                                             if (Character.isAlphabetic(lname_Low.codePointAt(0))){
                                                 builder_lname.setCharAt(0, Character.toUpperCase(lname_Low.charAt(0)));
                                                 norm_lname = builder_lname.toString();
                                             }


                                             String oldDateString = example.response.get(i).birthday;

                                             if(oldDateString != null){
                                                 Pattern pattern = Pattern.compile("\\d{4}-\\d{2}-\\d{2}"); // yyyy-mm-dd format
                                                 if (pattern.matcher(oldDateString).matches()) {
                                                     SimpleDateFormat oldDateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
                                                     SimpleDateFormat newDateFormat = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());

                                                     Date date = null;
                                                     try {
                                                         if(oldDateString == null){
                                                         }else{
                                                             date = oldDateFormat.parse(oldDateString);
                                                             String result = newDateFormat.format(date);
                                                             birthday = result;
                                                         }
                                                     } catch (ParseException e) {
                                                         e.printStackTrace();
                                                     }
                                                 }else{
                                                     birthday = oldDateString;
                                                 }
                                             }else{
                                                 birthday = "-";
                                             }
                                             User_mass.add(norm_name + "\t" + norm_lname + "\t" +
                                                     birthday + "\t" + example.response.get(i).avatrUrl + "\t" +
                                                             example.response.get(i).specialty.get(l).specialtyId + "\t" + example.response.get(i).specialty.get(l).name);
                                         }
                                     }else{

                                         String name_rand_case = example.response.get(i).fName;
                                         String lname_rand_case = example.response.get(i).lName;

                                         String name_Low = name_rand_case.toLowerCase();
                                         String lname_Low = lname_rand_case.toLowerCase();

                                         StringBuilder builder = new StringBuilder(name_Low);
                                         if (Character.isAlphabetic(name_Low.codePointAt(0))){
                                             builder.setCharAt(0, Character.toUpperCase(name_Low.charAt(0)));
                                             norm_name = builder.toString();
                                         }

                                         StringBuilder builder_lname = new StringBuilder(lname_Low);
                                         if (Character.isAlphabetic(lname_Low.codePointAt(0))){
                                             builder_lname.setCharAt(0, Character.toUpperCase(lname_Low.charAt(0)));
                                             norm_lname = builder_lname.toString();
                                         }

                                         String oldDateString = example.response.get(i).birthday;

                                         if(oldDateString != null){
                                             Pattern pattern = Pattern.compile("\\d{4}-\\d{2}-\\d{2}"); // yyyy-mm-dd format
                                             if (pattern.matcher(oldDateString).matches()) {

                                                 SimpleDateFormat oldDateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
                                                 SimpleDateFormat newDateFormat = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());

                                                 Date date = null;
                                                 try {
                                                     if(oldDateString == null){
                                                         Log.d(TAG, oldDateString + "--" );
                                                     }else{
                                                         date = oldDateFormat.parse(oldDateString);
                                                         String result = newDateFormat.format(date);
                                                         birthday = result;
                                                     }
                                                 } catch (ParseException e) {
                                                     e.printStackTrace();
                                                 }
                                             }else{
                                                 birthday = oldDateString;
                                             }
                                         }else{
                                             birthday = "-";
                                         }

                                         User_mass.add(norm_name + "\t" + norm_lname + "\t" +
                                                 birthday + "\t" + example.response.get(i).avatrUrl + "\t" +
                                                 example.response.get(i).specialty.get(0).specialtyId + "\t" + example.response.get(i).specialty.get(0).name);

                                     }


                                 }

                                 int num_user = User_mass.size();
                                 User users[] = new User[num_user];
                                 for(int q = 0; q<num_user; q++){
                                     String [] mass = User_mass.get(q).split("\t");
                                     users[q] = new User(q, mass[0],mass[1], mass[2], mass[3], mass[4],mass[5]);
                                 }
                                 insert(users);
                    }

                    @Override
                    public void onFailure(Call<Example> call, Throwable t) {
                    }
                });

        return newsData;
    }

    void insert(User[] users) {
        AppsDatabase.databaseWriteExecutor.execute(() -> {
            mUserDao.insertAll(Arrays.asList(users));
        });
    }
}