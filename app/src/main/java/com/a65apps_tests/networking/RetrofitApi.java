package com.a65apps_tests.networking;

import com.a65apps_tests.networking.model.Example;

import retrofit2.Call;
import retrofit2.http.GET;

public interface RetrofitApi {
    @GET("testTask.json")
    Call<Example> example();
}
