package com.a65apps_tests.networking.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Example extends Response {
    @SerializedName("response")
    @Expose
    public List<Response> response = null;

}
