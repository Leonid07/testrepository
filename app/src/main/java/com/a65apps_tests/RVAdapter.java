package com.a65apps_tests;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;
import java.util.List;

public final class RVAdapter extends RecyclerView.Adapter<RVAdapter.PersonViewHolder> {
    MutableLiveData<RVSpecialty> liveData = new MutableLiveData<>();

    final class PersonViewHolder extends RecyclerView.ViewHolder {

        TextView name;

        PersonViewHolder(View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.name);
        }

        void bind(RVSpecialty person) {
            name.setText(person.name);
            itemView.setOnClickListener(v -> liveData.postValue(person));
        }
    }

    private List<RVSpecialty> persons;

    RVAdapter(List<RVSpecialty> persons) {
        this.persons = persons;
    }

    @Override
    public PersonViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item, viewGroup, false);
        PersonViewHolder pvh = new PersonViewHolder(itemView);
        return pvh;
    }

    @Override
    public void onBindViewHolder(PersonViewHolder personViewHolder, int i) {
        personViewHolder.bind(persons.get(i));
    }

    @Override
    public int getItemCount() {
        return persons.size();
    }
}