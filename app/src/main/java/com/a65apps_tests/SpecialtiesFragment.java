package com.a65apps_tests;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.a65apps_tests.model.User;
import com.a65apps_tests.viewModel.UserViewModel;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link SpecialtiesFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SpecialtiesFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    final String TAG ="myLog";



    private RecyclerView rv;
    private List<RVSpecialty> persons;
    private String[] Name;
    private int siz;
    private UserViewModel userViewModel;

    public SpecialtiesFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment SpecialtiesFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static SpecialtiesFragment newInstance(String param1, String param2) {
        SpecialtiesFragment fragment = new SpecialtiesFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onViewCreated(@NonNull final View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        userViewModel = new ViewModelProvider(getActivity()).get(UserViewModel.class);

        userViewModel.getAllUser().observe(getViewLifecycleOwner(), new Observer<List<User>>() {
            @Override
            public void onChanged(List<User> users) {

                int kol = users.size();
                String[] Special_name = new String[kol];
                for (int i = 0; i < kol; i++) {
                    Special_name[i] = users.get(i).getSpecialty_name();
                }


                Set<String> set = new HashSet<String>(Arrays.asList(Special_name));
                final String[] result = set.toArray(new String[set.size()]);


                rv=(RecyclerView) view.findViewById(R.id.rv);

                LinearLayoutManager llm = new LinearLayoutManager(getContext());
                rv.setLayoutManager(llm);
                rv.setHasFixedSize(true);

                siz = result.length;

                Name = new String[siz];

                for (int i = 0; i < siz; i++) {
                    Name[i] = result[i];
                }

                initializeData();
                initializeAdapter();
            }
        });
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_specialties, container, false);
    }

    private void initializeData(){

        persons = new ArrayList<>();

        String data1;

        for (int i = 0; i < siz; i++){
            data1 = Name[i];
            persons.add(new RVSpecialty(data1));
        }
    }

    private void initializeAdapter(){
        RVAdapter adapter = new RVAdapter(persons);
        rv.setAdapter(adapter);

        adapter.liveData.observe(this, specialty -> {

            Bundle bundle=new Bundle();
            bundle.putString("name", specialty.name);

            WorkersFragment workersFragment = new WorkersFragment();
            workersFragment.setArguments(bundle);

            getActivity().getSupportFragmentManager().beginTransaction()
                    .replace(R.id.fragment_container, workersFragment)
                    .addToBackStack(null)
                    .commit();
        });
    }
}